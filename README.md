#### git

git add . / 文件名 / -A 提交到暂存区
git commit -m "message 提交信息" 提交到历史版本区

git log 查看历史记录

clone
ssh 长期有效的会话协议，需要配置公钥和私钥
https 一次浏览器会话协议，需要输入用户名和密码（凭据管理器）

ssh-keygen 生成公钥，在第三方服务上配置公钥

git reset 

git revert


> 创建分支

1. git checkout -b branchanme(分支名)
创建并切换分支 branch&checkout
2. git branch branchanme(分支名)
创建分支
3. git checkout branchanme(分支名)
切换分支
4. git merge 要合并的分支 （切换到目标分支）
5. git status 查看文件状态 （工作区干净 才能切分支）
6. git stash 暂存文件 （开发到一半，想看一下其他的代码逻辑，把自己当前开发一半的暂存起来）
7. git stash pop 回到上个暂存状态

### 项目基础建设
1. 技术选型
- 团队成员技术栈 保证项目进度
- 性能角度考虑 （虚拟dom减少dom操作，diff）
- 生态环境

2. react 整体项目规范制定
- 语法命名规范 
1. eslint
const run = “run”;
- 代码风格 
const a = "a";
 function run(){
console.log('11');
 }
- 提交规范
- 整体目录规划
- 全局样式 
- 接口跨域、项目基础配置（别名、less全局样式注入）

----
1. eslint
- 删除packageJson的eslintConfig字段
- 添加.eslintrc.js文件
```
extends
rules
plugin
```
----
2. preitter
1. 下载prettier^2.4.1" 可以使用prettier指令
2. 下载eslint-config-prettier 添加到eslintrc -》 extends语法包中
3. eslint-plugin-prettier 解决eslint和preitter冲突

--- 
3. husky
约束代码提交规范的

git

pre-commit 在提交之前检查代码 通过lint-staged 监控所有文件 执行eslint --fix 进行代码修复

commit-msg `npx husky add .husky/commit-msg "npx --no-install commitlint --edit $1" `

commitlint 需要下载

1. git commit -m "feat: 新增评论"
2. git commit -m "fix: 修复切换背景颜色的问题"
3. git commit -m "styles: 增加公共样式"
4. git commit -m "docs: 修改文档"




1. jsx是什么？
- 是React.createElement的语法糖，通过babel进行编译的
- 返回值是虚拟dom
2. 什么是虚拟dom
- 描述dom节点的js对象
3. 为什么会出现虚拟dom
- 节省dom开销,直接操作dom会引起页面的回流和重绘
- 优化diff算法比较 ，只比较关键属性（key）
- 跨平台开发 可以在nodejs中使用虚拟dom用来爬取页面数据，reactNative(开发app)
4. 目录规范
5. 配置别名（webpack-》resolve-》alias）
6. css modules 解决组件内样式,替换ui组件库样式  https://webpack.docschina.org/loaders/css-loader/#modules

react hooks
只能在函数组件中使用,所有的hook只能在函数组件的根作用域中使用

1. useState(initValue || ()=>initValue)
- 作用： 定义状态
- 返回值： 数组 [initValue,改变值的回调函数]
- 调用该函数，函数状态发生变化 函数组件重新加载

2. useEffect(()=>{})
- 副作用
useEffect(()=>{},[]) 第二个参数为空数组 等价于 componentDidMounted
useEffect(()=>{},[依赖状态]) 当依赖状态发生改变重新执行 componentDidMounted + componentDidupdate

3. useRef 

4. useImperativeHandle

Component 普通组件

React.PureComponent & React.memo

shouldComponentUpdated(nextProps){
  if(this.props === nextProps){
    return false
  }
  return true
}

1. 挂载阶段
- constructor
super(props)
this 设置初始state
- componentWillMount
- render 
返回虚拟dom
- componentDidMount
执行一次 初始渲染完成dom节点在这可以获取dom createRef useRef(只能在函数式组件使用)

2. 更新
shouldComponentUpdate(nextProps,nextState){
  this.props  this.state
  return true
}
componentWillUpdate
render
componentDidUpdate
setState(,() => {

})

3. 卸载
componentWillUnmount

4. 监听组件错误
static getDerivedStateFromError(error){
  return {}
}
componentDidCatch

5. useCallback
6. useMemo  
7. useReducer




sentry 检测错误的第三方平台（一般用来主动上报错误）
 
1. 项目背景
2. 项目成员介绍（职责介绍） 命名
3. 项目基础建设 
4. 项目亮点 主题切换 中英文切换 xxx组件封装 业务逻辑复用 第三方登录 markdown详情渲染 响应式（media js根据屏幕宽度判断 bootstrap 栅格布局）
5. 项目展示
6. 项目总结（做项目中的收获）
