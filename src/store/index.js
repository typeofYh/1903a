import articleStore from './modules/article';

const Store = {
    articleStore,
};

export default Store;
