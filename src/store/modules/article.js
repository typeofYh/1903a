import { makeAutoObservable } from 'mobx';
import { getRecommend } from '@/api/modules/article';
class ArticleStore {
    list = [];
    page = 1;
    getRecommend = async (page = 1, pageSize = 12) => {
        this.page = page;
        const data = await getRecommend({ page, pageSize });
        this.list = this.list.concat(data.data);
    };
}

export default makeAutoObservable(new ArticleStore());
