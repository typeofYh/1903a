import { HomeNav } from '@/router/RouterConfig';
import { NavLink } from 'react-router-dom';
import style from './style.module.less';
import classnames from 'classnames';
import { Component } from 'react';
const isLight = () => {
    const now = new Date();
    return now.getHours() > 6 && now.getHours() < 18;
};
class NoteHeader extends Component {
    state = {
        theme: isLight() ? '白' : '黑',
    };

    componentDidMount() {
        this.setDocumentClass();
    }
    setDocumentClass = () => {
        document.documentElement.className = this.state.theme === '白' ? 'light' : 'dark';
    };
    setTheme = () => {
        this.setState(
            {
                theme: this.state.theme === '白' ? '黑' : '白',
            },
            () => {
                this.setDocumentClass();
            }
        );
    };
    render() {
        const { theme } = this.state;
        return (
            <header>
                <ul className={classnames(style.nav)}>
                    {HomeNav.map(({ meta, path }) => {
                        return (
                            meta.nav && (
                                <li key={path}>
                                    <NavLink to={path}>{meta.navText}</NavLink>
                                </li>
                            )
                        );
                    })}
                </ul>
                <button
                    onClick={() => {
                        this.setTheme();
                    }}
                >
                    {theme}
                </button>
            </header>
        );
    }
}
export default NoteHeader;
