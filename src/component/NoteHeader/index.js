import { HomeNav } from '@/router/RouterConfig';
import { NavLink } from 'react-router-dom';
import style from './style.module.less';
import classnames from 'classnames';
import { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import i18n from '@/i18n';
const isLight = () => {
    const now = new Date();
    return now.getHours() > 6 && now.getHours() < 18;
};
const NoteHeader = () => {
    const { t } = useTranslation();
    const [theme, setTheme] = useState(() => (isLight() ? '白' : '黑'));
    const [lang, setLang] = useState(() => navigator.language);
    useEffect(() => {
        document.documentElement.className = theme === '白' ? 'light' : 'dark';
    }, [theme]);
    return (
        <header>
            <ul className={classnames(style.nav)}>
                {HomeNav.map(({ meta, path }) => {
                    return (
                        meta.nav && (
                            <li key={path}>
                                <NavLink to={path}>{t(meta.navText)}</NavLink>
                            </li>
                        )
                    );
                })}
            </ul>
            <button
                onClick={() => {
                    setTheme(theme === '白' ? '黑' : '白');
                }}
            >
                {theme}
            </button>
            <button
                onClick={() => {
                    const newlang = lang === 'en' ? 'zh-CN' : 'en';
                    setLang(newlang);
                    i18n.changeLanguage(newlang);
                    console.log(i18n);
                }}
            >
                {lang}
            </button>
        </header>
    );
};

export default NoteHeader;
