import { useEffect, useRef, forwardRef, useImperativeHandle, memo } from 'react';
import _ from 'lodash';
const ScrollLoad = (props, ref) => {
    // console.log('ref', ref);
    const scrollEvent = useRef(null);
    const parentDom = useRef(null);
    useImperativeHandle(ref, () => {
        return {
            // current
            removeScroll() {
                window.removeEventListener('scroll', scrollEvent.current);
            },
        };
    });
    useEffect(() => {
        scrollEvent.current = _.throttle(() => {
            const clientHeight = document.documentElement.clientHeight;
            const scrollTop = document.documentElement.scrollTop;
            const pageHeight = parentDom.current.children[0].clientHeight;
            if (clientHeight + scrollTop > pageHeight - 40) {
                props.handleUpload && props.handleUpload();
            }
        }, 500);
        window.addEventListener('scroll', scrollEvent.current);
        return () => {
            window.removeEventListener('scroll', scrollEvent.current);
        };
    }, []);
    return <div ref={parentDom}>{props.children}</div>;
};

export default memo(forwardRef(ScrollLoad));
