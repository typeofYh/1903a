import classNames from 'classnames';
import style from './style.module.less';
import loadingImg from '@/assets/images/loading.gif';
import { useEffect, useState } from 'react';
const NoteBlock = ({
    title = 'title',
    itemRender = () => null,
    getData = () => {},
    className = '',
}) => {
    const [data, setData] = useState([]);
    const [loadingFlag, setLoadingFlag] = useState(false);
    useEffect(() => {
        getData().then((res) => {
            setData(res.data);
            setLoadingFlag(true);
        });
    }, [getData]);
    return (
        <div className={classNames(style['noteBlock'], className)}>
            <h2>{title}</h2>
            <div>
                {loadingFlag ? (
                    Array.isArray(data) ? (
                        data.map((item) => itemRender(item))
                    ) : (
                        <div>暂无{title}数据</div>
                    )
                ) : (
                    <p>
                        <img src={loadingImg} alt="" />
                    </p>
                )}
            </div>
        </div>
    );
};
export default NoteBlock;
