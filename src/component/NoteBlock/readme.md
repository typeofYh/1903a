### 组件作用

- 优化推荐阅读 等模块的调用方式，结合业务逻辑

### 组件使用

```jsx
<NoteBlock
    title="推荐阅读"
    getData={getComment}
    itemRender={({ id, title, createAt }) => (
        <div key={id}>
            <span>{title}</span>.<b>{getLastTime(createAt)}</b>
        </div>
    )}
/>
```

- props说明

| props | 是否必填 | 说明
|---|---|---|
| title | 是 | 标题展示 支持string |
| getData | 是 | 请求数据函数 返回值必须是promise
| itemRender | 是 | 内容渲染函数 返回渲染的内容 接受 rowdata（行数据）|

