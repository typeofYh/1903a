import { Component, Fragment } from 'react';
import { sendError } from '@/utils/utils';
class HandleError extends Component {
    state = {
        hasError: false,
    };
    static getDerivedStateFromError() {
        return {
            hasError: true,
        };
    }
    componentDidCatch() {
        // 错误上报
        sendError({
            msg: '组件内部渲染错误',
            type: 0,
        });
    }
    render() {
        const { hasError } = this.state;
        return (
            <Fragment>
                {hasError ? (
                    <div>
                        <h1>组件发生未知错误</h1>
                    </div>
                ) : (
                    this.props.children
                )}
            </Fragment>
        );
    }
}

export default HandleError;
