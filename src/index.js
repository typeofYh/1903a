import React from 'react';
import ReactDom from 'react-dom';
import App from './App';
import '@/i18n';
import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';

Sentry.init({
    dsn: 'https://6b353e2376d9429c813a2be31550d95a@o958342.ingest.sentry.io/6069993',
    integrations: [new Integrations.BrowserTracing()],
    environment: process.env.NODE_ENV,
    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
});
console.log('sentry', Sentry);
ReactDom.render(<App />, document.querySelector('#root'));
