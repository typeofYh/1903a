import style from './style.module.less';
import classname from 'classnames';
import NoteHeader from '@/component/NoteHeader';
import RouterView from '@/router/RouterViews';
const HomePage = ({ RouterConfig }) => {
    return (
        <div className={classname(style.home, 'container')}>
            <NoteHeader />
            <main>
                <RouterView RouterConfig={RouterConfig} />
            </main>
        </div>
    );
};

export default HomePage;
