/*
 * @Author: your name
 * @Date: 2021-11-11 15:27:08
 * @LastEditTime: 2021-11-19 16:20:55
 * @LastEditors: your name
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: /1903A/1903a/src/pages/NotFind/index.js
 */
import { Component } from 'react';
class NotFind extends Component {
    state = {
        time: 3,
    };
    timer = null;
    componentDidMount() {
        this.timer = setInterval(() => {
            if (this.state.time < 1) {
                clearInterval(this.timer);
                this.handleToHome();
                return;
            }
            this.setState(({ time }) => ({
                time: --time,
            }));
        }, 1000);
    }
    handleToHome = () => {
        this.props.history.replace('/');
    };
    componentWillUnmount() {
        clearInterval(this.timer);
    }
    render() {
        const { time } = this.state;
        return (
            <div>
                <p>页面走丢了～</p>
                <p>{time}秒之后跳转</p>
                <button onClick={this.handleToHome}>点击跳转首页</button>
            </div>
        );
    }
}

export default NotFind;
