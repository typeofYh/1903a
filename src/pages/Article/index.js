/*
 * @Author: your name
 * @Date: 2021-11-12 17:03:33
 * @LastEditTime: 2021-11-19 16:48:21
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: /1903A/1903a/src/pages/Article/index.js
 */
import { getComment, getTag } from '@/api/modules/article';
import NoteBlock from '@/component/NoteBlock';
import { getLastTime } from '@/utils/utils';
import classNames from 'classnames';
import { inject, observer } from 'mobx-react';
import { useState } from 'react';
import ScrollLoad from '@/component/ScrollLoad';
import { useRef, useCallback, useMemo } from 'react';
import { useHistory } from 'react-router-dom';
let initToDetail = null;
const Article = (props) => {
    const [falg, setFalg] = useState(0);
    const history = useHistory();
    useState(() => {
        props.articleStore.getRecommend();
    }, []);
    const scrollLoad = useRef(null);
    console.log('render', falg);
    const toDetail = useCallback(() => {
        // console.log(falg, 'useCallback', history);
        // 重新定义
        history.push('/detail');
    }, []);
    const list = useMemo(() => {
        console.log('list-------value');
        return new Array(falg).fill('').map((item, index) => index + 1);
    }, [falg]);
    console.log('list', list);
    if (falg === 0) {
        initToDetail = toDetail;
    }
    console.log('对比函数', initToDetail === toDetail);
    return (
        <ScrollLoad
            ref={scrollLoad}
            handleUpload={() => {
                if (props.articleStore.page >= 2) {
                    scrollLoad.current.removeScroll();
                    return;
                }
                props.articleStore.getRecommend(props.articleStore.page + 1);
            }}
        >
            {list.map((item) => (
                <div>{item}------</div>
            ))}
            <button onClick={() => setFalg((val) => val + 1)}>sendError{falg}</button>
            <div className={classNames('list')}>
                {props.articleStore.list.map((item, i) => (
                    <div key={i}>{item.content}</div>
                ))}
                <div>loading...</div>
            </div>
            <NoteBlock
                title="推荐阅读"
                getData={getComment}
                itemRender={({ id, title, createAt }) => (
                    <div key={id} onClick={toDetail}>
                        <span>{title}</span>.<b>{getLastTime(createAt)}</b>
                    </div>
                )}
            />
            <NoteBlock
                title="文章标签"
                getData={getTag}
                itemRender={({ id, value, articleCount }) => (
                    <div key={id}>
                        <span>{value}</span>[<b>{articleCount}</b>]
                    </div>
                )}
            />
        </ScrollLoad>
    );
};
export default inject('articleStore')(observer(Article));
