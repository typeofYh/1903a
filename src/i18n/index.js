import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import en from './config/en.config';
import ch from './config/ch.config';

const resources = {
    en: { translation: en },
    'zh-CN': { translation: ch },
};
i18n.use(initReactI18next) // passes i18n down to react-i18next
    .use(LanguageDetector)
    .init({
        resources,
        detection: {
            order: [
                'querystring',
                'cookie',
                'localStorage',
                'sessionStorage',
                'navigator',
                'htmlTag',
                'path',
                'subdomain',
            ],
        },
        interpolation: {
            escapeValue: false, // react already safes from xss
        },
    });

export default i18n;
