import axios from '@/utils/httpTool';

export const getComment = () => axios.get('/api/article/recommend');
export const getTag = () => axios.get('/api/tag');
console.log(process.env);
export const getRecommend = (params: { page: 1, pageSize: 12 }) =>
    axios.get(process.env.REACT_APP_TEST_BASEURL + '/api/article/all/recommend', { params });

// cors 跨域资源共享 服务端解决
// proxy 反向代理解决跨域

// 开发过程中请求后端本地接口
// 上线之后请求线上正式接口

// 1. 网络请求超时情况
// 2. 开发环境和生成环境的根路径 http:后端ip地址   https://creationapi.shbwyz.com
// 3. httpcode状态码 200 400 403 处理公共错误状态
// 4. 请求前配置公共的headers或者其他配置
