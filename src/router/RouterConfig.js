import { lazy } from 'react';
export const HomeNav = [
    {
        path: '/home/article',
        meta: {
            title: '文章｜八维创作平台',
            navText: 'article',
            nav: true,
        },
        component: lazy(() => import('pages/Article')),
    },
    {
        path: '/home/archive',
        meta: {
            title: '归档｜八维创作平台',
            navText: '归档',
            nav: true,
        },
        component: lazy(() => import('pages/End')),
    },
    {
        path: '/home/book',
        meta: {
            title: '知识小测｜八维创作平台',
            navText: '知识小测',
            nav: true,
        },
        component: lazy(() => import('pages/Article')),
    },
];
const RouterConfig = [
    {
        to: '/home',
        from: '/',
    },
    {
        path: '/home',
        name: 'Home',
        component: lazy(() => import('pages/Home')),
        children: HomeNav,
    },
    {
        path: '*',
        component: lazy(() => import('pages/NotFind')),
    },
];
export default RouterConfig;
