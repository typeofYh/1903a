import { Suspense } from 'react';
import { Route } from 'react-router-dom';
import { BrowserRouter, HashRouter, Switch, Redirect } from 'react-router-dom';
export const setTitle = (title) => {
    document.title = title;
};
const RouterView = ({ RouterConfig = [] }) => {
    const notFind = RouterConfig.find((item) => item.path === '*');
    return (
        <Suspense fallback={<h3>Loading....</h3>}>
            <Switch>
                {RouterConfig.filter((item) => item.component && item.path !== '*')
                    .map((item) => {
                        return (
                            <Route
                                key={item.path}
                                path={item.path}
                                render={(routerProps) => {
                                    item.meta && setTitle(item.meta.title || '八维创作平台');
                                    return (
                                        <item.component
                                            {...routerProps}
                                            RouterConfig={item.children || []}
                                        />
                                    );
                                }}
                            />
                        );
                    })
                    .concat(
                        RouterConfig.filter((item) => item.to).map(({ from, to }) => {
                            return (
                                <Route
                                    strict
                                    exact
                                    key={from}
                                    path={from}
                                    render={() => <Redirect to={to} />}
                                />
                            );
                        }),
                        notFind ? (
                            <Route
                                key={'*'}
                                path="*"
                                render={(props) => <notFind.component {...props} />}
                            />
                        ) : null
                    )}
            </Switch>
        </Suspense>
    );
};

export const Routers = ({ mode = 'history', RouterConfig = [] }) => {
    return mode === 'history' ? (
        <BrowserRouter>
            <RouterView RouterConfig={RouterConfig} />
        </BrowserRouter>
    ) : (
        <HashRouter>
            <RouterView RouterConfig={RouterConfig} />
        </HashRouter>
    );
};
export default RouterView;
