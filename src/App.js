import classname from 'classnames';
import RouterConfig from '@/router/RouterConfig';
import { Routers } from '@/router/RouterViews';
import '@/styles/common.less';
import '@/styles/theme.css';
import store from '@/store';
import { Provider } from 'mobx-react';
import HandleError from '@/component/HandleCatch';

const App = () => {
    return (
        <HandleError>
            <Provider {...store}>
                <div className={classname('root')}>
                    <Routers RouterConfig={RouterConfig} />
                </div>
            </Provider>
        </HandleError>
    );
};
export default App;
