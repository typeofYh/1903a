import axios from 'axios';
import { sendError } from '@/utils/utils';
const httpTool = axios.create({
    timeout: 10000,
});

/**
 * [description]
 * const xhr = new XMLHttpRequest();
 * xhr.open(method,url,async);
 * xhr.send();
 * xhr.onreadystatechange = () => {
 *  if(xhr.readyState === 4){
 *    调响应拦截器
 *    if(xhr.status === 200){
 *       console.log(xhr.response)
 *    } else {
 *    }
 *  } else {
 *    console.log('loading....');
 *  }
 * }
 */

httpTool.interceptors.request.use(
    // xhr.open
    (config) => {
        console.log(config);
        //请求前配置公共的headers或者其他配置
        return config; // 返回合法配置
    },
    (error) => {
        return Promise.reject(error);
    }
);
httpTool.interceptors.response.use(
    //
    (data) => {
        return data.data;
    },
    (error) => {
        sendError({
            msg: error.url + '接口错误',
            type: 1,
        });
        return Promise.reject('testerror');
    }
);

export default httpTool;
