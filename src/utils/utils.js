import * as Sentry from '@sentry/react';
export const getLastTime = (createAt) => {
    const createTime = new Date(createAt);
    const times = new Date() - createTime;
    const s = times / 1000 / 60 / 60 / 24;
    return s > 0 ? s.toFixed() + '天前' : s;
};

export const sendError = ({ msg, type }) => {
    //上报sentry
    Sentry.captureMessage(`错误类型：${type}，错误信息：${msg}`);
};
